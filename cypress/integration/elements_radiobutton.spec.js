const url = '/radio-button'

describe('Elements radio button', () => {
  before(() => {
    cy.visit(url)
    cy.url()
      .should('eq', Cypress.config().baseUrl + url)
  })

  it('all elements unchecked', () => {
    cy.get('#yesRadio.custom-control-input').should('not.be.checked');
    cy.get('#impressiveRadio.custom-control-input').should('not.be.checked');
    cy.get('#noRadio.custom-control-input[disabled]').should('exist');
    
  })

  it('first element checked', () => {
    cy.get('[for="yesRadio"]').click()
    cy.get('#yesRadio.custom-control-input').should('be.checked');
    cy.get('#impressiveRadio.custom-control-input').should('not.be.checked');
    cy.get('#noRadio.custom-control-input[disabled]').should('exist');
    cy.get('.text-success').should('contain', 'Yes')
  })

  it('second element checked', () => {
    cy.get('[for="impressiveRadio"]').click()
    cy.get('#yesRadio.custom-control-input').should('not.be.checked');
    cy.get('#impressiveRadio.custom-control-input').should('be.checked');
    cy.get('#noRadio.custom-control-input[disabled]').should('exist');
    cy.get('.text-success').should('contain', 'Impressive')
  })

})