const url = '/webtables'

function addFormElement({
  firstName = 'John',
  lastName = 'Doe',
  userEmail = 'john.doe@qq.com',
  age = '29',
  salary = '35000',
  department = 'management'
} = {}) {
  cy.get('#addNewRecordButton').click()

  cy.get('#firstName').type(firstName)
  cy.get('#lastName').type(lastName)
  cy.get('#userEmail').type(userEmail)
  cy.get('#age').type(age)
  cy.get('#salary').type(salary)
  cy.get('#department').type(department)

  cy.get('#submit').click()

  cy.get('.rt-tbody').should('contain', firstName)
  cy.get('.rt-tbody').should('contain', lastName)
  cy.get('.rt-tbody').should('contain', userEmail)
  cy.get('.rt-tbody').should('contain', age)
  cy.get('.rt-tbody').should('contain', salary)
  cy.get('.rt-tbody').should('contain', department)
}

describe('Web Tables', () => {
  before(() => {
    cy.visit(url)
    cy.url()
      .should('eq', Cypress.config().baseUrl + url)
  })

  it('submit empty form validation', () => {
    cy.reload()
    
    cy.get('#addNewRecordButton').click()
    cy.get('#submit').click()

    cy.get('#firstName-wrapper .form-control:invalid').should('exist')
    cy.get('#lastName-wrapper .form-control:invalid').should('exist')
    cy.get('#userEmail-wrapper .form-control:invalid').should('exist')
    cy.get('#age-wrapper .form-control:invalid').should('exist')
    cy.get('#salary-wrapper .form-control:invalid').should('exist')
    cy.get('#department-wrapper .form-control:invalid').should('exist')
  })

  it('delete button removes all the elements', () => {
    cy.reload()
    
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 3)

    cy.get('#delete-record-3').click()
    cy.get('#delete-record-2').click()
    cy.get('#delete-record-1').click()

    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 0)
  })

  it('search narrows elements in the table while typing', () => {
    cy.reload()
    
    cy.get('#searchBox').type('Cierra')

    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 1)
  })

  it('add one element', () => {
    cy.reload()

    addFormElement()

    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 4)
  })

  it('add many elements for two pages', () => {
    cy.reload()

    for (let i=1; i<=8; i++) {
      addFormElement()
    }

    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 10)
    cy.get('.-totalPages').should('contain', '2')
  })

  it('add many elements then remove element to reduce amount of pages', () => {
    cy.reload()

    for (let i=1; i<=8; i++) {
      addFormElement()
    }

    cy.get('.-totalPages').should('contain', '2')
    cy.get('#delete-record-10').click()
    cy.get('.-totalPages').should('contain', '1')
  })
  
})