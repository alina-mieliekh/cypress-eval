const url = '/books'

function navigateToBookDetails(bookName) {
  //cy.get('.rt-tbody').contains(bookName).click({force: true})
  cy.get('.rt-tbody').contains(bookName).click('left')
  cy.url().should('not.eq', Cypress.config().baseUrl + url)
  cy.get('#title-wrapper #userName-value').should('contain', bookName)
  cy.get('#addNewRecordButton').should('contain', 'Back To Book Store').click()
  cy.url().should('eq', Cypress.config().baseUrl + url)
}

describe('Book Store', () => {
  before(() => {
    cy.visit(url)
    cy.url().should('eq', Cypress.config().baseUrl + url)
  })

  it('check initial amount of books', () => {
    cy.intercept("GET", `${Cypress.config().baseUrl}/BookStore/v1/Books`).as("bookList")
    cy.reload()
    cy.wait("@bookList").then((interception) => {
      const books = interception.response.body.books

      expect(books.length).to.equal(8)
      expect(books[0].title).to.equal('Git Pocket Guide')
      expect(books[1].author).to.equal('Addy Osmani')
      expect(books[2].pages).to.equal(238)
    })
    
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 8)
  })

  it("Visit first book details", () => {
    cy.reload()
    cy.intercept("GET", `${Cypress.config().baseUrl}/BookStore/v1/Books`).as("bookList")
    cy.contains("Git Pocket Guide").click('left');
    cy.intercept("GET", `${Cypress.config().baseUrl}/BookStore/v1/Book?ISBN=9781449325862`).as("book1")
    cy.reload()
    cy.wait("@book1").then((interception) => {
      const info = interception.response.body

      expect(info.author).to.equal('Richard E. Silverman')
      expect(info.publisher).to.equal('O\'Reilly Media')
      expect(info.pages).to.equal(234)
      cy.get('#addNewRecordButton').should('contain', 'Back To Book Store').click()
    })
  })

  it('go to book details', () => {
    cy.reload()
    
    const books = [
      'Git Pocket Guide',
      'Learning JavaScript Design Patterns',
      'Designing Evolvable Web APIs with ASP.NET'
    ];

    books.forEach(bookName => {
      navigateToBookDetails(bookName)
    })
  })

  it('search narrows amount of books shown', () => {
    cy.reload()
    cy.get('#searchBox').type('JavaScript')
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 4)
  })

  it('search by author surname', () => {
    cy.reload()
    cy.get('#searchBox').type('Simpson')
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 1)
  })

  it('change items per page to 5, increase amount of pages to 2', () => {
    cy.reload() 
    cy.get('.-pageSizeOptions select').select('5 rows')
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 5)
    cy.get('.-totalPages').should('contain', 2)
  })

  it('click Next button', () => {
    cy.reload()
    cy.get('.-next button').should('be.disabled')
    cy.get('.-pageSizeOptions select').select('5 rows')
    cy.get('.-next button').click()
    cy.get('.rt-tbody .rt-tr:not(.-padRow)').should('have.length', 3)
    cy.get('.-pageJump input').should('have.value', 2)
  })


})