const url = '/text-box'

describe('Elements text box', () => {
  before(() => {
    cy.visit(url)
    cy.url()
      .should('eq', Cypress.config().baseUrl + url)
  })

  it('validation of failed email field', () => {
    const invalidEmails = ['name@example.co$m', 'nam!e@example.com', 'nameexample.com', 'name@examplecom'];

    invalidEmails.forEach(email => {
        cy.get('#userEmail').clear().type(email)
        cy.get('#submit').click()
        cy.get('#userEmail.field-error')
    })
  })

  it('validation of correct email field', () => {

    cy.get('#userEmail').clear().type('name@example.com')
    cy.get('#submit').click()
    cy.get('#userEmail').should('not.have.class', 'field-error')
  })  

  it('successful submit', () => {
    const address = 'Henry Hernandez\n Notting Estate\n 123 Notting Lane\n Nottingham\n NG1 1AJ\n England';

    cy.get('#userName').clear().type('Name Surname')
    cy.get('#userEmail').clear().type('name@example.com')
    cy.get('#currentAddress').clear().type(address)
    cy.get('#permanentAddress').clear().type(address)
    cy.get('#submit').click()
    cy.get('#output #name').should('contain', 'Name Surname')
    cy.get('#output #email').should('contain', 'name@example.com')
    cy.get('#output #currentAddress').should('contain', address)
    cy.get('#output #permanentAddress').should('contain', address)
  })  
})