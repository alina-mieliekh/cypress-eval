const url = '/checkbox'

describe('Elements check box', () => {
  before(() => {
    cy.visit(url)
    //cy.reload()
    cy.url()
      .should('eq', Cypress.config().baseUrl + url)
  })

  it('expand node tree', () => {
    cy.get('.rct-option.rct-option-expand-all').click()
    cy.get('#tree-node-veu ~ .rct-title').should('contain', 'Veu')
    cy.get('#tree-node-downloads ~ .rct-title').should('contain', 'Downloads')
  })
  it('collapse node tree', () => {
    cy.get('.rct-icon.rct-icon-collapse-all').click()
    cy.get('#tree-node-veu ~ .rct-title').should('not.exist')
    cy.get('#tree-node-downloads ~ .rct-title').should('not.exist')
  })

  it('Child and parent nodes unchecked', () => {
    cy.get('.rct-option.rct-option-expand-all').click()
    cy.get('#tree-node-public ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-private ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-classified ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-general ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-office ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-documents ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-home ~.rct-checkbox .rct-icon-uncheck').should('exist')
  })

  it('Child and parent nodes partial check', () => {
    cy.get('#tree-node-public ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-public ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#tree-node-private ~.rct-checkbox .rct-icon-uncheck').should('exist')
    cy.get('#tree-node-office ~.rct-checkbox .rct-icon-half-check').should('exist')
    cy.get('#tree-node-documents ~.rct-checkbox .rct-icon-half-check').should('exist')
    cy.get('#tree-node-home ~.rct-checkbox .rct-icon-half-check').should('exist')
    cy.get('#result .text-success').should('contain', 'public')
  })

  it('Child checked and parent nodes partially checked', () => {
    cy.reload()
    cy.get('.rct-option.rct-option-expand-all').click()
    cy.get('#tree-node-public ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-private ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-classified ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-general ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-office ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#tree-node-documents ~.rct-checkbox .rct-icon-half-check').should('exist')
    cy.get('#tree-node-home ~.rct-checkbox .rct-icon-half-check').should('exist')
    cy.get('#result .text-success').should('contain', 'office')
    cy.get('#result .text-success').should('contain', 'public')
    cy.get('#result .text-success').should('contain', 'private')
    cy.get('#result .text-success').should('contain', 'classified')
    cy.get('#result .text-success').should('contain', 'general')
  })

  it('GrandChild checked and parent nodes fully checked', () => {
    cy.reload()
    cy.get('.rct-option.rct-option-expand-all').click()
    cy.get('#tree-node-notes ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-commands ~.rct-checkbox .rct-icon-uncheck').click()

    cy.get('#tree-node-workspace ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-office ~.rct-checkbox .rct-icon-uncheck').click()

    cy.get('#tree-node-wordFile ~.rct-checkbox .rct-icon-uncheck').click()
    cy.get('#tree-node-excelFile ~.rct-checkbox .rct-icon-uncheck').click()
    
    cy.get('#tree-node-desktop ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#tree-node-documents ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#tree-node-downloads ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#tree-node-home ~.rct-checkbox .rct-icon-check').should('exist')
    cy.get('#result .text-success').should('contain', 'home')

  })
})